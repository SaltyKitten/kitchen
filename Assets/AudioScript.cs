﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {
    public AudioSource audioBottom;
    public AudioSource audioMedium;
    public AudioSource audioTop;
    public AudioSource audioUi;

    public AudioClip uiTick;

    public AudioClip[] happyMonster;
    public AudioClip[] angryMonster;

    public static AudioScript instance;

    private void Awake() {
        instance = this;
    }

    public  void PlayMonsterLoose(int positionY) {
        GetManagerByLane(positionY).PlayOneShot(angryMonster[UnityEngine.Random.Range(0, angryMonster.Length)]);
    }

    public void PlayMonsterWin(int positionY) {
        GetManagerByLane(positionY).PlayOneShot(happyMonster[UnityEngine.Random.Range(0, happyMonster.Length)]);
    }

    private AudioSource GetManagerByLane(int y) {
        if (y == 0)
            return audioBottom;
        if (y == 1)
            return audioMedium;
        if (y == 2)
            return audioTop;
        else
            throw new Exception();



    }
}
