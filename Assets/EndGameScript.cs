﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameScript : MonoBehaviour {

    public Text nbGoodText;
    public Text title;
    public Image Gordon;
    
    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void ShowEnd(int nbGood) {
        this.gameObject.SetActive(true);
        nbGoodText.text = nbGood.ToString();
        title.text = returnTitle(nbGood);



    }

    private string returnTitle(int nbGood) {
        if (nbGood <= 5)
            return "Commis";
        else if (nbGood <= 14)
            return "Sous Chef";
        else if (nbGood <= 24)
            return "Chef";
        else if (nbGood <= 39)
            return "Top Chef";
        else if (nbGood <= 59)
            return "Pape de la gastronomie";
        else {
            Gordon.gameObject.SetActive(true);

            
            
            return "Gordon Ramsay Approval";

        }
    }

}
