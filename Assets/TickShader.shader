﻿Shader "Unlit/TickShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorA ("colorA", COLOR) = (0,0,0,0)
		_ColorB ("colorB", COLOR) = (0,0,0,0)
		_Progress ("Progress", Range(0,1)) = 0.25
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _ColorA;
			float4 _ColorB;
			float _Progress;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture

				fixed4 col = lerp (_ColorA,_ColorB,ceil(i.uv.x-_Progress ));
				// apply fog
				
				return col;
			}
			ENDCG
		}
	}
}
