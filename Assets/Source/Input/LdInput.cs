﻿using Framework.Utils;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using UnityInput = UnityEngine.Input;

using LibelludDigital.Math;

namespace LibelludDigital.Input
{
    public class LdInput : Singleton<LdInput>
    {
        #region Unity

        private void Awake()
        {
            Init();
        }

        protected override void OnDestroy()
        {
            OnKill();
            base.OnDestroy();
        }

        private void Update()
        {
            Tick();
        }

        private void LateUpdate()
        {
            // DEBUG
            if(_dragDebugDisplayMaterial != null)
            {
                _dragDebugDisplayMaterial.SetFloat("_Direction", Mathf.Sin(Time.realtimeSinceStartup) * 0.5f + 0.5f);
                _dragDebugDisplayMaterial.SetColor("_Color", Random.ColorHSV(0, 1));
                Graphics.DrawMesh(
                    _dragDebugDisplayMesh,
                    Matrix4x4.identity,
                    _dragDebugDisplayMaterial,
                    0
                    );
            }
        }
        #endregion

        #region Debug
        Mesh        _dragDebugDisplayMesh;
        Material    _dragDebugDisplayMaterial;

        public Shader dragDebugDisplayShader;


        private void InitDebugDisplay()
        {
            // Create Quad
            _dragDebugDisplayMesh = new Mesh();
            _dragDebugDisplayMesh.vertices = new Vector3[]
            {
                new Vector3(-0.5f, -0.5f, 5f),
                new Vector3( 0.5f, -0.5f, 5f),
                new Vector3(-0.5f,  0.5f, 5f),
                new Vector3( 0.5f,  0.5f, 5f)
            };
            _dragDebugDisplayMesh.uv = new Vector2[]
            {
                new Vector3(0, 0),
                new Vector3(1, 0),
                new Vector3(0, 1),
                new Vector3(1, 1)
            };
            _dragDebugDisplayMesh.triangles = new int[]
            {
                0, 2, 3,
                3, 1, 0
            };

            // Create Material
            Shader dragDebugDisplayShader = Shader.Find("");
            if(dragDebugDisplayShader != null)
            {
                _dragDebugDisplayMaterial = new Material(Shader.Find(""));// dragDebugDisplayShader);
                // _dragDebugDisplayMaterial = new Material(Shader.Find("Unlit/Color"));
            }
            else
            {
 
            }
        }
        #endregion

        #region Init / Kill
        private void Init()
        {
            TouchInit();
            InitDebugDisplay();
        }

        private void Kill()
        {
            Destroy(gameObject);
        }

        private void OnKill()
        {

        }
        #endregion

        #region Tick
        private void Tick()
        {
            //MouseKeyboardTick();
            //TouchTick();
            isAnyDrag = false;
#if UNITY_IOS || UNITY_ANDROID
            TouchTick();
#endif
#if !(UNITY_IOS || UNITY_ANDROID) || UNITY_EDITOR
            FakeTouchTick();
#endif

        }
        #endregion

        #region Mouse & Keyboard

        private Vector2 _v2CurrentMousePosition;
        private Vector2 _v2LastFrameMousePosition;
        private Vector2 _v2CurrentMouseDelta;
        //private bool    _bLeftClickDown;

        private void MouseKeyboardTick()
        {
            _v2CurrentMousePosition = UnityInput.mousePosition;
            _v2CurrentMouseDelta = _v2LastFrameMousePosition - _v2CurrentMousePosition;
            if (UnityInput.GetMouseButtonDown(0))
            {
                //OnSwipeBeggin(UnityInput.mousePosition);
            }

            if (UnityInput.GetMouseButtonUp(0))
            {
                //OnSwipeEnd(UnityInput.mousePosition, new Vector2(UnityInput.GetAxis("Horizontal"), UnityInput.GetAxis("Vertical")) * Screen.width);
            }

            if (UnityInput.GetMouseButton(0))
            {
                //OnSwipeTick(UnityInput.mousePosition);
            }
            _v2LastFrameMousePosition = _v2CurrentMousePosition;
        }
        #endregion // Mouse & Keyboard

        #region Mouse & Keyboard (Fake)

        #endregion

        #region Touch

        // Events
        public static   TouchEvent  eOnTouchBegin;
        public static   TouchEvent  eOnTouchTick;
        public static   TouchEvent  eOnTouchEnd;

        public static   TouchEvent  eOnDragBegin;
        public static   TouchEvent  eOnDragTick;
        public static   TouchEvent  eOnDragEnd;
        
        public static   bool        isAnyDrag; // Mostly used for Android hack ...

        // Fields
        private const   int         _nMaxTouch       = 1;
        private         LdTouch[]   _touches;

        private void TouchInit()
        {
            _touches = new LdTouch[_nMaxTouch];
            for (int iTouch = 0; iTouch < _nMaxTouch; iTouch++)
            {
                _touches[iTouch] = new LdTouch();
                _touches[iTouch].id = iTouch;
            }
        }

        private void TouchTick()
        {
            // Unity
            int nCurrentUnityTouchCount = UnityInput.touchCount;
            UnityEngine.Touch unityTouch;

            // Libellud Digital
            int iTouch; LdTouch touch;


            // Unity -> Libellud Digital
            for (int iUnityTouch = 0; iUnityTouch < Mathf.Min(nCurrentUnityTouchCount, _nMaxTouch); ++iUnityTouch)
            {
                unityTouch = UnityInput.GetTouch(iUnityTouch);

                iTouch = unityTouch.fingerId;
                if (iTouch >= _nMaxTouch) continue;  // Skip touch if too many fingers on screen.
                touch = _touches[unityTouch.fingerId];

                TickTouchInstance(ref touch, unityTouch);
            };
        }
        
        private void TickTouchInstance(ref LdTouch touch, UnityEngine.Touch unityTouch)
        {
            // STATE
            touch.phase = (LdTouch.Phase)unityTouch.phase;
            touch.lastPosition = touch.position;
            touch.position = unityTouch.position;

            // TIME
            touch.dt = unityTouch.deltaTime;

            if (touch.phase == LdTouch.Phase.BEGAN)
            {
                // STATE
                touch.originalPosition  = touch.position;
                touch.delta             = Vector2.zero;
                touch.deltaOrigin       = Vector2.zero;
                touch.isActive          = true;
                touch.isDragging        = false;

                // TIME
                touch.startTime         = Time.realtimeSinceStartup;
                touch.lifetime          = 0;

                // VELOCITY
                touch.velocity          = Vector2.zero;
            }
            else
            {
                // STATE
                touch.delta             = unityTouch.deltaPosition;
                touch.deltaOrigin       = touch.position - touch.originalPosition;

                // TIME
                touch.lifetime          = Time.realtimeSinceStartup - touch.startTime;

                // VELOCITY
                if (touch.dt != 0) // Don't event try to refresh veocity if it requires dividing by zero...
                {
                    const float smoothing = 0.33f; // Keep part of last frame velocity for cheap smoothing.
                    touch.velocity      = Vector2.Lerp((touch.delta / touch.dt), touch.velocity, smoothing * 0.5f);
                }
            }
            if (touch.phase == LdTouch.Phase.ENDED || touch.phase == LdTouch.Phase.ENDED)
            {
                touch.isActive          = false;
            }

            // DRAG / SWIPE
            TickTouchInstanceDrag(ref touch);

            // CALLBACKS
            TickTouchInstanceCallbacks(ref touch);

        }

        private void TickTouchInstanceDrag(ref LdTouch touch)
        {
            if (touch.id != 0) return;
            // DRAG / SWIPE
            if (touch.isDragging)
            {
                isAnyDrag = true;
                if (touch.phase == LdTouch.Phase.ENDED || touch.phase == LdTouch.Phase.CANCELED)
                {
                    touch.dragPhase = LdTouch.DragPhase.ENDED;
                    touch.isDragging = false;
                }
                else
                {
                    touch.dragPhase = (LdTouch.DragPhase)touch.phase;
                }
            }
            else
            {
                if (
                    touch.isActive &&
                    touch.deltaOrigin.magnitude * LdMath.PixelsToCentimeters > LdTouch.DeltaOriginInCentimeterToBeExededToBeginADragSwipe
                    )
                {
                    // A Drag/Swipe just began
                    if (Mathf.Abs(touch.deltaOrigin.x) >= Mathf.Abs(touch.deltaOrigin.y))
                    {
                        touch.swipeAxis = LdTouch.SwipeAxis.HORIZONTAL;
                    }
                    else
                    {
                        touch.swipeAxis = LdTouch.SwipeAxis.VERTICAL;
                    }

                    touch.dragPhase = LdTouch.DragPhase.BEGAN;
                    touch.isDragging = true;
                }
                else
                {
                    touch.dragPhase = LdTouch.DragPhase.CANCELED;
                }
            }
        }

        public void TickTouchInstanceCallbacks(ref LdTouch touch)
        {
            // TOUCH CALLBACK
            switch (touch.phase)
            {
                case LdTouch.Phase.BEGAN:
                    if (eOnTouchBegin != null)
                    {
                        eOnTouchBegin(touch);
                    }
                    break;
                case LdTouch.Phase.MOVED:
                case LdTouch.Phase.STATIONARY:
                    if (eOnTouchTick != null)
                    {
                        eOnTouchTick(touch);
                    }
                    break;
                case LdTouch.Phase.ENDED:
                    if (eOnTouchEnd != null)
                    {
                        eOnTouchEnd(touch);
                    }
                    break;
            }

            // DRAG CALLBACK
            switch (touch.dragPhase)
            {
                case LdTouch.DragPhase.BEGAN:
                    if (eOnDragBegin != null)
                    {
                        eOnDragBegin(touch);
                    }
                    //((StandaloneInputModule)EventSystem.current.currentInputModule).DeactivateModule();
                    //EventSystem.current.currentInputModule.DeactivateModule(); // Hack
                    //EventSystem.current.currentInputModule.DeactivateModule(); // Hack works when !Android, use isAnyTouchDraggin hack when on Android platform
                    //eventSystem.enabled = false;
                    
                    // Feedback
                    if(_dragDebugDisplayMaterial != null)
                    {
                        _dragDebugDisplayMaterial.SetVector("_Offset", Camera.main.ScreenToWorldPoint(touch.position));
                        _dragDebugDisplayMaterial.SetFloat("_Direction", Mathf.Atan2(touch.deltaOrigin.y, touch.deltaOrigin.x));
                    }
                    break;
                case LdTouch.DragPhase.MOVED:
                case LdTouch.DragPhase.STATIONARY:
                    if (eOnDragTick != null)
                    {
                        eOnDragTick(touch);
                    }
                    //EventSystem.current.CancelInvoke();
                    break;
                case LdTouch.DragPhase.ENDED:
                    if (eOnDragEnd != null)
                    {
                        eOnDragEnd(touch);
                    }
                    break;
            }
        }

        #endregion

        #region Touch (Fake)

        private void FakeTouchTick()
        {
            LdTouch touch = _touches[0];
            
            _v2CurrentMousePosition     = UnityInput.mousePosition;
            _v2CurrentMouseDelta        = _v2CurrentMousePosition - _v2LastFrameMousePosition;

            // STATE
            touch.lastPosition          = _v2LastFrameMousePosition;
            touch.position              = _v2CurrentMousePosition;

            // TIME
            touch.dt = Time.deltaTime;

            touch.phase = LdTouch.Phase.CANCELED;

            if (UnityInput.GetMouseButtonDown(0))
            {
                touch.phase             = LdTouch.Phase.BEGAN;

                // STATE
                touch.originalPosition  = touch.position;
                touch.delta             = Vector2.zero;
                touch.deltaOrigin       = Vector2.zero;
                touch.isActive          = true;
                touch.isDragging        = false;

                // TIME
                touch.startTime         = Time.realtimeSinceStartup;
                touch.lifetime          = 0;

                // VELOCITY
                touch.velocity          = Vector2.zero;
            }
            else
            {
                // STATE
                touch.delta = _v2CurrentMouseDelta;
                touch.deltaOrigin = touch.position - touch.originalPosition;

                // TIME
                touch.lifetime = Time.realtimeSinceStartup - touch.startTime;

                // VELOCITY
                if (touch.dt != 0) // Don't event try to refresh veocity if it requires dividing by zero...
                {
                    const float smoothing = 0.33f; // Keep part of last frame velocity for cheap smoothing.
                    touch.velocity = Vector2.Lerp((touch.delta / touch.dt), touch.velocity, smoothing * 0.5f);
                }

                if (UnityInput.GetMouseButton(0))
                {
                    if (_v2CurrentMouseDelta != Vector2.zero)
                    {
                        touch.phase = LdTouch.Phase.MOVED;
                    }
                    else
                    {
                        touch.phase = LdTouch.Phase.STATIONARY;
                    }
                }

            }
            if (UnityInput.GetMouseButtonUp(0))
            {
                touch.phase     = LdTouch.Phase.ENDED;
                touch.isActive  = false;
            }

            // DRAG
            TickTouchInstanceDrag(ref touch);

            // CALLBACKS
            TickTouchInstanceCallbacks(ref touch);

            _v2LastFrameMousePosition = _v2CurrentMousePosition;
        }
        #endregion

        #region Utils
        public void CancelEventSystem()
        {
            EventSystem.current.SetSelectedGameObject(null);
        }

        /// <summary>
        /// Please use the variant with a GraphicRaycaster as parameter, it's more efficient.
        /// </summary>
        public void RaycastTouchUi(LdTouch touch, Canvas canvas, ref List<RaycastResult> raycastResults)
        {
            GraphicRaycaster gr = canvas.GetComponent<GraphicRaycaster>();
            PointerEventData ped = new PointerEventData(null);
            ped.position = touch.position;
            gr.Raycast(ped, raycastResults);
        }

        public void RaycastTouchUi(LdTouch touch, GraphicRaycaster graphicRaycaster, ref List<RaycastResult> raycastResults)
        {
            PointerEventData ped = new PointerEventData(null);
            ped.position = touch.position;
            graphicRaycaster.Raycast(ped, raycastResults);
        }
        #endregion
    }
}