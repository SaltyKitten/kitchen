﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LibelludDigital.Math;

namespace LibelludDigital.Input
{
    public delegate void TouchEvent(LdTouch t);

    public class LdTouch
    {
        /// <summary>
        /// When the user's finger started moving too far from it's original position, indicates witch axis was the primary one.
        /// </summary>
        public enum SwipeAxis
        {
            NONE,
            /// <summary>
            /// When the user's finger started moving too far from it's original position, his movement was primary on the horizontal axis.
            /// </summary>
            HORIZONTAL,
            /// <summary>
            /// When the user's finger started moving too far from it's original position, his movement was primary on the vertical axis.
            /// </summary>
            VERTICAL
        }

        /// <summary>
        /// Describes the stages of touch lifetime.
        /// </summary>
        public enum Phase
        {
            /// <summary>
            /// A finger touched the screen.
            /// </summary>
            BEGAN = 0,
            /// <summary>
            /// A finger is touching the screen and is moving.
            /// </summary>
            MOVED = 1,
            /// <summary>
            /// A finger is touching the screen and isn't moving.
            /// </summary>
            STATIONARY = 2,
            /// <summary>
            /// A finger was lifted from the screen. This is the final phase of a touch.
            /// </summary>
            ENDED = 3,
            /// <summary>
            /// The system cancelled tracking for the touch.
            /// </summary>
            CANCELED = 4     // #JCC ='(
        }

        /// <summary>
        /// Describes the stages of drag lifetime.
        /// </summary>
        public enum DragPhase
        {
            /// <summary>
            /// A finger just moved too far from it's original position when it first touched the screen.
            /// </summary>
            BEGAN = 0,
            /// <summary>
            /// A drag is being performed and the finger is moving.
            /// </summary>
            MOVED = 1,
            /// <summary>
            /// A drag is being performed and the finger isn't moving.
            /// </summary>
            STATIONARY = 2,
            /// <summary>
            /// A drag has just ended.
            /// </summary>
            ENDED = 3,
            /// <summary>
            /// The system cancelled tracking for the touch.
            /// </summary>
            CANCELED = 4
        }

        public int id = -1;

        /// <summary>
        /// In witch stage of his lifetime this touch is.
        /// </summary>
        public Phase phase;

        /// <summary>
        /// In witch stage of his lifetime is the drag performed by this touch.
        /// </summary>
        public DragPhase dragPhase;

        /// <summary>
        /// The position of the touch at last frame. (in pixels)
        /// </summary>
        public Vector2 lastPosition;

        /// <summary>
        /// The current position of the touch. (in pixels)
        /// </summary>
        public Vector2 position;

        /// <summary>
        /// The position of the touch when it began. (in pixels)
        /// </summary>
        public Vector2 originalPosition;

        /// <summary>
        /// The distance travelled by the touch, since last frame. (in pixels)
        /// </summary>
        public Vector2 delta;

        /// <summary>
        /// The distance between the current touch position and it's original one. (in pixels)
        /// </summary>
        public Vector2 deltaOrigin;

        internal const float DeltaOriginInCentimeterToBeExededToBeginADragSwipe = 0.25f;
        internal const float InchToCentimeter = 2.54f;

        /// <summary>
        /// When the user's finger started moving too far from it's original position. Witch axis seemed to be the primary one.
        /// </summary>
        public SwipeAxis swipeAxis;

        /// <summary>
        /// The aplication lifetime (in seconds) when this input started.
        /// </summary>
        public float startTime;

        /// <summary>
        /// The elapsed time (in seconds) since the moment this input started.
        /// </summary>
        public float lifetime;

        /// <summary>
        /// Dark Templar... Or the time (in seconds) since when this input was last handled.
        /// </summary>
        public float dt;

        /// <summary>
        /// The velocity of the finger. (in pixels per seconds)
        /// </summary>
        public Vector2 velocity;

        /// <summary>
        /// Is the unser's finger touching the screen.
        /// </summary>
        public bool isActive = false;

        /// <summary>
        /// Has user's finger moved too far from it's original position.
        /// </summary>
        public bool isDragging = false;

        /// <summary>
        /// Projects the input position on a plane facing the camera in world space.
        /// </summary>
        /// <param name="distance">The distance of the plane facing the camera.</param>
        /// <returns></returns>
        Vector3 PositionWorld(Camera camera, float distance)
        {
            Vector3 projection = position;
            projection.z = distance;
            return camera.ScreenToWorldPoint(position);
        }

        /// <summary>
        /// Projects the input position on a plane in world space.
        /// </summary>
        /// <param name="planeOrigin"></param>
        /// <param name="planeNormal"></param>
        /// <returns></returns>
        Vector3 PositionWorld(Camera camera, Vector3 planeOrigin, Vector3 planeNormal)
        {
            throw new System.NotImplementedException();
        }

        public static Vector2 PositionAnchor(RectTransform rectTransform, Vector2 cursorPositionScreen)
        {
            Canvas c = rectTransform.GetComponentInParent<Canvas>();
            RectTransform r = (RectTransform)c.transform;
            Vector2 canvasSize = r.rect.size;
            Vector2 realSize = c.pixelRect.size;
            Vector2 scaleRatio = new Vector2(realSize.x / canvasSize.x, realSize.y / canvasSize.y);
            Rect rect = rectTransform.rect;
            rect.position -= rect.position;
            rect.Set(rect.x * scaleRatio.x, rect.y * scaleRatio.y, rect.width * scaleRatio.x, rect.height * scaleRatio.y);
            return LdMath2d.InverseLerp(rect.min, rect.max, cursorPositionScreen); ;
        }

        #region Init & Tick
        internal void Init(UnityEngine.Touch unityTouch)
        {
            phase = (Phase)unityTouch.phase;
            originalPosition = position = unityTouch.position;
            velocity = deltaOrigin = delta = Vector2.zero;
            startTime = Time.realtimeSinceStartup;
            swipeAxis = SwipeAxis.NONE;
            lifetime = 0;
            dt = unityTouch.deltaTime;
        }

        internal void Tick(UnityEngine.Touch unityTouch, out bool dragSwipeBegan)
        {
            phase = (Phase)unityTouch.phase;
            position = unityTouch.position;
            delta = unityTouch.deltaPosition;
            deltaOrigin = position - originalPosition;
            dragSwipeBegan = false;
            if (swipeAxis == SwipeAxis.NONE)
            {
                float deltaOriginToBeExededToBeginADragSwipe =
                    Screen.dpi * DeltaOriginInCentimeterToBeExededToBeginADragSwipe * InchToCentimeter;
                if (deltaOrigin.magnitude > deltaOriginToBeExededToBeginADragSwipe)
                {
                    // A Drag/Swipe began
                    dragSwipeBegan = true;
                    if (deltaOrigin.x >= deltaOrigin.y)
                    {
                        swipeAxis = SwipeAxis.HORIZONTAL;
                    }
                    else
                    {
                        swipeAxis = SwipeAxis.VERTICAL;
                    }
                }
            }

            swipeAxis = SwipeAxis.NONE;
            lifetime = Time.realtimeSinceStartup - startTime;
            dt = unityTouch.deltaTime;
            // Refresh velocity
            if (dt != 0) // Don't divide by zero...
            {
                const float smoothing = 0.33f; // Keep part of last frame velocity for cheap smoothing.
                velocity = Vector2.Lerp((delta / dt), velocity, smoothing * 0.5f);
            }

        }
        #endregion
    }

    public static class LdTouchUtils
    {
        public static Vector2 GetVector(this LdTouch.SwipeAxis swipeAxis)
        {
            switch (swipeAxis)
            {
                case LdTouch.SwipeAxis.HORIZONTAL:
                    return Vector2.right;
                case LdTouch.SwipeAxis.VERTICAL:
                    return Vector2.up;
                default:
                    throw new System.NotSupportedException();
            }
        }

    }
}