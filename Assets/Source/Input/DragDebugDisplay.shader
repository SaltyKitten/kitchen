// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit alpha-blended shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "LibelludDigital/Input/DragDebugDisplay" {
Properties {
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	[Toggle(UNITY_FOG)] _UseFog("Use Fog", Float) = 0
}

SubShader {
    Tags {
		"Queue"="Overlay"
		"IgnoreProjector"="True"
		"RenderType"="Transparent"
	}
    LOD 100

	ZTest Always
    ZWrite Off
    Blend SrcAlpha OneMinusSrcAlpha

    Pass {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
		
			#pragma multi_compile __ UNITY_FOG

			#define UN_SUR_PI 0.31830988618379067153776752674503

            struct VS_INPUT {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct FS_INPUT {
                float4 vertex : SV_POSITION;
                float2 texcoord : TEXCOORD0;
				#ifdef UNITY_FOG
                UNITY_FOG_COORDS(1)
				#endif
                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			uniform float4 _Color;
			uniform float4 _Offset;
			uniform float _Direction;

			FS_INPUT vert (VS_INPUT i)
            {
				FS_INPUT o;
                UNITY_SETUP_INSTANCE_ID(i);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(i.vertex + _Offset);
                o.texcoord = TRANSFORM_TEX(i.texcoord, _MainTex);
				#ifdef UNITY_FOG
                UNITY_TRANSFER_FOG(o,o.iertex);
				#endif
                return o;
            }

            fixed4 frag (FS_INPUT i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.texcoord);
				
				float2 uv = i.texcoord;
				//uv = uv * 2 - 1;

				float value = sin(_Time.y) * 0.5 + 0.5;

				float2 uveffect = uv * 2 - 1;


				float v = lerp(2, 4, value);

				float effect = atan2(uveffect.y, uveffect.x);
				effect = effect * UN_SUR_PI * 0.5 + 0.5;
				effect += _Direction;//_Direction;//  *-0.25;
				//effect += 1;
				//effect *= 1;
				effect = effect - floor(effect);
				//effect *= 0.5;
				//effect += value;
				//effect = (effect) * 4;
				float d = 1 / value * 1;
				effect = effect * 2 * d - d;
				effect = effect * effect;

				uv = uv * 2 - 1;
				float a = 1 - length(uv);
				//a *= 1 -value;
				//effect *= value;
				//effect = a;
				//a = smoothstep(0, .05,  a);
				effect = clamp(effect, 0, 1);
				a = clamp(a, 0, 1);
				effect = lerp(a, effect, value);// *lerp(a, effect, value);
				effect = smoothstep(0.8, 1.00, effect);
				//effect = a;
				//effect = step(effect, 0.5);
				//effect = smoothstep(lerp(0, 0.8, value), lerp(0.1, 1, value), effect);
				a = clamp(a, 0, 1);
				effect *= 1-smoothstep(0.25, 1, length(uv));
				//effect = lerp(effect, a, value);
				//effect = effect / 3.14;
				//effect = effect * 0.5 + 0.5;

				//effect += value;
				//effect *= 0.5;
				//effect =  (1 + effect) % 1;
				//effect = 1 - (1 - effect) * (1 - effect);
				//effect = smoothstep(0.5, 0.5, effect);
				
				//col.rgb = 0;
				//col.rg = i.texcoord;
				//col.b = effect;
				//col.r = a;
				col.rgb *= _Color.rgb;
				col.a = effect;

				#ifdef UNITY_FOG
                UNITY_APPLY_FOG(i.fogCoord, col);
				#endif
                return col;
            }
        ENDCG
    }
}

}
