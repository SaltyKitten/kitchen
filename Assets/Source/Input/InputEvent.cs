﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LibelludDigital.Input.Event
{
    // Input event
    public delegate void InputBeggin();
    public delegate void InputTick(ref float duration, ref float dt);
    public delegate void InputEnd(ref float duration);

    // Interaction event
    public delegate void InteractionBeggin(ref Collider collider);
    public delegate void InteractionTick(ref Collider collider, ref float duration, ref float dt);
    public delegate void InteractionEnd(ref Collider collider, ref float duration);

}
