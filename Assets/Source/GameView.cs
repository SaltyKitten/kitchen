﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameView : MonoBehaviour {

    public static GameView instance;


    public float MOVEMENT_RESOLUTION_TICK_DURATION      = 0.5f;
    public float COMBINAISON_RESOLUTION_TICK_DURATION   = 0.25f;
    public float STATE_RESOLUTION_TICK_DURATION         = 0.25f;

    public float TOTAL_CYCLE_DURATION;

    public LifeBarScript lifebar;
    public GameModel model;
    public GameObject counter;
    public UnityEngine.UI.Text goodText;

    public EndGameScript endGameScript;

    public GameObject entityPrefab;
    public GameObject cardViewPrefab;
    public GameObject[] lines;

    public MonsterView[] monsterSpots;
    private MonsterView[] _monstersOnSpots;
    private MonsterView[] _monsterIncomings;
    private float _lastTick;


    private Material _counterMaterial;
    public int distanceRespawnMonsters = 4;


    public HandModule handView;

    public AnimationCurve monsterSpawnCurve;

    private void Awake() {
        instance = this;
        TOTAL_CYCLE_DURATION = MOVEMENT_RESOLUTION_TICK_DURATION;// + COMBINAISON_RESOLUTION_TICK_DURATION + STATE_RESOLUTION_TICK_DURATION;
        EntityView.MOVEMENT_RESOLUTION_TICK_DURATION = MOVEMENT_RESOLUTION_TICK_DURATION;
        EntityView.COMBINAISON_RESOLUTION_TICK_DURATION = COMBINAISON_RESOLUTION_TICK_DURATION;
        EntityView.STATE_RESOLUTION_TICK_DURATION = STATE_RESOLUTION_TICK_DURATION;
    }


    public void Start() {
        _monstersOnSpots = new MonsterView[3];
        _monsterIncomings = new MonsterView[3];
        _counterMaterial = counter.GetComponent<Renderer>().material;
        model = new GameModel();
        model.Init();


        lifebar.Init(model.lives);
        for (int i = 0; i < 3; i++) {
            if (model.monsters[i] != null) {
                _monstersOnSpots[i] = Instantiate(PrefabRefs.instance.GetMonsterPrefab(), monsterSpots[i].transform.position, Quaternion.identity).GetComponent<MonsterView>();
                _monstersOnSpots[i].Init(model.monsters[i]);

            }
        }

        handView.cardViews = new CardView[model.hand.Length];
        model.eOnCardDrawn += OnCardDrawn;

        model.DrawMissingCards();

        handView.eOnCardDropOutsideHand = OnCardDrop;

        StartCoroutine(TickCoroutine());
    }

    public void OnCardDrawn(int iCard) {
        // Make card view
        CardView cardView = Instantiate(cardViewPrefab, handView.transform).GetComponent<CardView>();
        cardView.transform.localPosition = Vector3.up * -2f;
        cardView.Init(model.hand[iCard]);
        handView.cardViews[iCard] = cardView;
        handView.Organise(0.5f, true);
    }

    public void OnCardDrop(int iCard) {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 20f)) {
            CardSlotView cardSlotView = hit.collider.GetComponent<CardSlotView>();
            if(cardSlotView != null) {

                Vector3 position = hit.collider.transform.position + Vector3.up * 0.120f  + Vector3.forward * -1f;
                handView.cardViews[iCard].transform.position = position;
                handView.cardViews[iCard].transform.localScale = Vector3.one * 0.85f;//.DOScale(0.85f, 0.5f);
                handView.cardViews[iCard].OnPlay();

                handView.cardViews[iCard] = null;
                model.PlayCard(iCard, cardSlotView.iSlotIndex);
                //return;
            }
        }
        handView.Enter();
    }

    public void Update() {
        _counterMaterial.SetFloat("_Progress", (Time.time - _lastTick) / MOVEMENT_RESOLUTION_TICK_DURATION);
        if (Input.GetKeyDown(KeyCode.R)) {
            // Create entity

            // Model
            EntityModel entityModel = model.MakeEntity();
            // View

        }
        
    }

    public void SpawnRandomCandies(int count) {
        for (int iCandy = 0; iCandy < count; iCandy++) {
            // Model
            EntityModel entityModel = model.MakeEntity();
            // View

            EntityView enityView = Instantiate(PrefabRefs.instance.entityPrefab).GetComponent<EntityView>();
            enityView.Init(entityModel);

            // Spawn entity
            // Model
            model.SpawnEntity(entityModel, 0, Random.Range(0, GameModel.CELL_COUNT_Y));
            // View
            Vector3 position = lines[entityModel.positionY].transform.GetChild(entityModel.positionX).position;
            enityView.transform.position = position - Vector3.right * 1f;
            enityView.transform.DOMove(position, MOVEMENT_RESOLUTION_TICK_DURATION).SetEase(Ease.Linear);
            enityView.Spawn(MOVEMENT_RESOLUTION_TICK_DURATION);
        }
    }


    public int turn = 0;
    private IEnumerator TickCoroutine() {
        
        while (true) {
            turn++;
            // Refresh dificulty
            MOVEMENT_RESOLUTION_TICK_DURATION = GetDificulty(turn);
            EntityView.MOVEMENT_RESOLUTION_TICK_DURATION = MOVEMENT_RESOLUTION_TICK_DURATION;
            EntityView.COMBINAISON_RESOLUTION_TICK_DURATION = COMBINAISON_RESOLUTION_TICK_DURATION * 0.25f;
            EntityView.STATE_RESOLUTION_TICK_DURATION = MOVEMENT_RESOLUTION_TICK_DURATION * 0.25f;

            model.TickMovementResolution();
            SpawnRandomCandies(Random.Range(0, 2));
            model.TickCardConsumption();
            _lastTick = Time.time;
            yield return new WaitForSeconds(MOVEMENT_RESOLUTION_TICK_DURATION);
            model.TickCombinaisonResolution();
            //yield return new WaitForSeconds(COMBINAISON_RESOLUTION_TICK_DURATION);
            if( !model.TickStateResolution())
                break;
            UpdateMonsters();
            //yield return new WaitForSeconds(STATE_RESOLUTION_TICK_DURATION);

        }
        yield return new WaitForSeconds(2);
        endGameScript.ShowEnd(model.goodPoints);

    }

    public float GetDificulty(int turn) {
        float output = 2f/((turn * 3f)/256f + 1);
        output = ((output * output) / 2.4f) + 0.4f;
        return output;
    }

    private void UpdateMonsters() {
        for (int i = 0; i < 3; i++) {
            if (_monstersOnSpots[i] == null && model.monsters[i] != null) {
                _monstersOnSpots[i] = _monsterIncomings[i];
                _monsterIncomings[i] = null;
            }
            else if (_monsterIncomings[i] == null && model.monsterApproachings[i] != null) {
                _monsterIncomings[i] = Instantiate(PrefabRefs.instance.GetMonsterPrefab(), monsterSpots[i].transform.position + new Vector3(-distanceRespawnMonsters, 0, 0), Quaternion.identity).GetComponent<MonsterView>();
                _monsterIncomings[i].Init(model.monsterApproachings[i]);
                float duration = 0;
                int delayTurn = (model.monsterApproachingDelays[i] + 1);
                for (int t = turn = 0; t < delayTurn; t = turn++) {
                    duration += GetDificulty(t);
                }
                _monsterIncomings[i].transform.DOMove(monsterSpots[i].transform.position, duration).SetEase(monsterSpawnCurve) ;
            }
            //if (_monstersOnSpots[i] == null && model.monsters[i] != null) {
            //    _monstersOnSpots[i] = Instantiate(PrefabRefs.instance.monsterPrefab, monsterSpots[i].transform.position, Quaternion.identity).GetComponent<MonsterView>();
            //}
        }


    }

    public void EntityEaten(int positionY, bool v) {
        if (v) {
            _monstersOnSpots[positionY].transform.DOShakePosition(0.75f, 0.02f, 5, 45, false, true);
            _monstersOnSpots[positionY].SetHappy();
            OnGoodEaten();
            AudioScript.instance.PlayMonsterWin(positionY);
        }
        else {
            _monstersOnSpots[positionY].transform.DOShakePosition(0.40f, 0.25f, 100, 45, false, true);
            _monstersOnSpots[positionY].SetAngry();
            lifebar.LooseLives();
            AudioScript.instance.PlayMonsterLoose(positionY);
        }
        //monsters[positionY].transform.DOShakeRotation(0.25f);
        StartCoroutine(AfterEaten(_monstersOnSpots[positionY]));
        _monstersOnSpots[positionY] = null;

    }
    private void OnGoodEaten() {
            this.goodText.text = "X " + model.goodPoints;
    }

    private IEnumerator AfterEaten(MonsterView monster) {

        yield return new WaitForSeconds(MOVEMENT_RESOLUTION_TICK_DURATION * 0.5f);
        monster.transform.DOMoveX(5, TOTAL_CYCLE_DURATION * 1).onComplete = delegate () {
           Destroy(monster.gameObject);
        };
    }
}
