﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveType
{
    MOVE,   // The entity should move this tick
    STAND   // The entity should stay still this tick$
}

public static class MoveTypeExtentions
{
    public static bool Is(this MoveType compared, MoveType comparaison)
    {
        return (compared & comparaison) == comparaison;
    }
}

public class CardModel {
    
    public int moveX, moveY;

    public bool movementEffectWasApplied = false;
    public bool consumed = false;

    public System.Action eOnConsomption;
    public System.Action eOnCancelTop;
    public System.Action eOnCancelBot;
    public System.Action eOnUnCancelTop;
    public System.Action eOnUnCancelBot;

    public bool cancelledTop = false;
    public bool cancelledBot = false;

    public enum MoveType : int {
        FREEZE,
        TOP_LEFT,
        TOP,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM,
        BOTTOM_RIGHT
    }

    public MoveType moveType;

    public void SetMoveType(MoveType newMoveType) {
        moveType = newMoveType;
        switch (newMoveType) {
            case MoveType.FREEZE:
                moveX = 0;
                moveY = 0;
                break;
            case MoveType.TOP_LEFT:
                moveX = -1;
                moveY = 1;
                break;
            case MoveType.TOP:
                moveX = 0;
                moveY = 1;
                break;
            case MoveType.TOP_RIGHT:
                moveX = 1;
                moveY = 1;
                break;
            case MoveType.BOTTOM_LEFT:
                moveX = -1;
                moveY = -1;
                break;
            case MoveType.BOTTOM:
                moveX = 0;
                moveY = -1;
                break;
            case MoveType.BOTTOM_RIGHT:
                moveX = 1;
                moveY = -1;
                break;
        }
    }
    
    public bool TryConsume() {
        if (movementEffectWasApplied) {
            Consume();
            return true;
        } else {
            return false;
        }
    } 

    public void Consume() {
        consumed = true;
        if (eOnConsomption != null)
            eOnConsomption();
    }

    public void CancelTop() {
        cancelledTop = true;
        if (eOnCancelTop != null)
            eOnCancelTop();
    }

    public void UnCancelTop() {
        cancelledTop = false;
        if (eOnCancelTop != null)
            eOnUnCancelTop();
    }

    public void CancelBot() {
        cancelledBot = true;
        if (eOnCancelBot != null)
            eOnCancelBot();
    }

    public void UnCancelBot() {
        cancelledBot = false;
        if (eOnCancelBot != null)
            eOnUnCancelBot();
    }

}
