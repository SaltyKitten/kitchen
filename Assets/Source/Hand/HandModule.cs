﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibelludDigital.Input;

using DG.Tweening;
using UnityEngine.UI;
using LibelludDigital.Input;
using LibelludDigital.Math;

public class HandModule : MonoBehaviour {

    public delegate void CardDropOutsideHandHandler(int iCard);

    public CardDropOutsideHandHandler eOnCardDropOutsideHand;

    public CardView[]      cardViews;

    private static float    _HandHitboxPick = 0.20f;
    private static float    _HandHitboxDrop = 0.15f;
    private float           _handHitbox = _HandHitboxPick;

    private int             iFocusedCard = -1;
    private int             iDraggedCard = -1;
    private float           _focusTime;

    [System.Obsolete]
    private bool            active = false;

    private bool            _isAlreadyChosen = false;
    private bool            _hasJustDragged = false;
    private Tweener         _pickTween;

    private void Awake() {
        Open(cardViews);
        Enter();
    }

    public bool GotFocusedCard() {
        return iFocusedCard >= 0;
    }
    public bool GotDraggedCard() {
        return iDraggedCard >= 0;
    }

    public void Open(CardView[] cardViews_) {
        eOnCardDropOutsideHand = null; // Clean Events
        this.cardViews = cardViews_;
    }

    public void Organise(float duration, bool high) {
        for (int iCardView = 0; iCardView < cardViews.Length; iCardView++) {
            if (cardViews[iCardView] == null) continue;
            cardViews[iCardView].transform.DOLocalMove(Vector3.right * (iCardView * 1.375f - 2.0f), 0.25f);
            cardViews[iCardView].transform.DOScale(1f, 0.125f);
        }
    }

    public void Close() {

    }

    public void Enter() {
        EnterView();
        _handHitbox = _HandHitboxPick; // This is important : So not to right hitbox on screen entering
        BindInputs();
        LdInput instance = LdInput.instance;//Hack
    }


    private void onTapCard(ref Collider collider, ref float duration) {
        throw new NotImplementedException();
    }

    public void Exit() {
        UnBindInputs();
        //pickHandCardLayout.Organise(_cardViews);
        ExitView();
    }

    #region View

    private void EnterView() {
        Organise(0.5f, true);
    }

    private void ExitView() {
    }

    public void FocusCard(int iCard) {
        if (GotFocusedCard()) {
            // A card is already focused.
            if (iFocusedCard != iCard) {
                // The new card is different from the curently focused one : Unfocus the old one and focus the new one.
                UnfocusCard(iFocusedCard, true);
            } else {
                // Don't do anythhing if card already focused.
                return;
            }
        } else {
            Organise(0.5f, false);
        }

        if (GotDraggedCard()) {
            // A card is currently dragged : Put it back to the hand...
            _isAlreadyChosen = false;
            PutBackCard(iDraggedCard);
        }
        cardViews[iCard].transform.DOKill();
        //cardViews[iCard].transform.DOLocalMove(Vector3.up * 4f + Vector3.forward * -1f, 0.25f);
        cardViews[iCard].transform.DOLocalMove(Vector3.right * (iCard * 1.375f - 2.0f) + Vector3.up * 0.125f, 0.25f);
        cardViews[iCard].transform.DOScale(1.125f, 0.125f);

        iFocusedCard = iCard;

        _handHitbox = _HandHitboxPick;
    }

    public void UnfocusCard(int iCard, bool reapplyLayout = true) {
        if (reapplyLayout)
            Organise(0.5f, true);
        iFocusedCard = -1;
        _hasJustDragged = false;
    }

    /// <summary>
    /// Called when the player pick a card from his hand.
    /// </summary>
    /// <param name="iCard"></param>
    public void PickCard(int iCard) {
        if (GotFocusedCard()) {
            // A card is curently focused : Unfocus it
            UnfocusCard(iFocusedCard, false);
        }

        if (GotDraggedCard()) {
            // A card is already beeing 
            if (iDraggedCard == iCard) {
                // The card is already beeing draged : Don't do anything.
                return;
            }
            // The curently dragged card is different from the new one : Put the old card back to the hand.
            PutBackCard(iDraggedCard);
        }
        _hasJustDragged = true;

        // Actyaly pick the card
        Organise(0.5f, false);

        iDraggedCard = iCard;

        DOTween.Kill(cardViews[iDraggedCard].transform);
        cardViews[iDraggedCard].transform.DORotate(Vector3.zero, 0.125f);
        //_cardViews[iDraggedCard].transform.DOScale(0.75f, 0.125f);
        cardViews[iDraggedCard].transform.localScale = Vector3.one * 0.75f;

        _handHitbox = _HandHitboxDrop;
    }

    /// <summary>
    /// Called each frame the player is holding a card outside of his hand.
    /// </summary>
    public void TickPickedCard(Vector2 mousePosition) {
        // He is holding a card

        Vector3 newPos = mousePosition;
        newPos.z = cardViews[iDraggedCard].transform.position.z - Camera.main.transform.position.z;
        newPos = Camera.main.ScreenToWorldPoint(newPos);
        newPos.y += 0.25f;
        newPos.z = -3;
        cardViews[iDraggedCard].transform.position = newPos;
    }

    /// <summary>
    /// Called when the player drop his card outside his hand.
    /// </summary>
    /// <param name="iCard"></param>
    public void DropCard(int iCard) {
        Exit();
        iDraggedCard = -1;

        if (eOnCardDropOutsideHand != null) {
            eOnCardDropOutsideHand(iCard);
        }
    }

    /// <summary>
    /// Called when the player put the card he is currently holding back to his hand.
    /// </summary>
    /// <param name="iCard"></param>
    public void PutBackCard(int iCard, bool reapplyLayout = true) {

        Organise(0.5f, false);

        iDraggedCard = -1;
    }

    #endregion // View

    #region Inputs

    private Vector2 _lastMousePosition;

    private const float TAP_TIME = 0.1f;

    public void BindInputs() {
        LdInput.eOnTouchTick = OnTouchTick;
        LdInput.eOnTouchEnd = OnTouchEnd;
    }

    public void UnBindInputs() {
        LdInput.eOnTouchTick -= OnTouchTick;
        LdInput.eOnTouchEnd -= OnTouchEnd;
    }

    public void OnTouchTick(LdTouch touch) {
        Vector2 normalisedMousePosition = LdMath2d.FromScreenToNormalisedScreenPoint(Screen.width, Screen.height, touch.position);

        _focusTime += Time.deltaTime;
        // The player hold his finger onto the screen
        if (normalisedMousePosition.y <= _handHitbox) {
            // He is hovering the hand hitbox
            if (touch.swipeAxis == LdTouch.SwipeAxis.HORIZONTAL || GotDraggedCard() || !GotFocusedCard()) {
                // He is moving horizontaly
                int iCardIndex = FromPositionToCardIndex(normalisedMousePosition.x);
                FocusCard(iCardIndex);
            }
        } else {
            // He isn't hovering the hand hitbox
            if (GotFocusedCard()) {
                // He draged a card outside his hand
                PickCard(iFocusedCard);
                AudioScript.instance.audioUi.PlayOneShot(AudioScript.instance.uiTick);
            }

            if (GotDraggedCard()) {
                // He is holding a card
                TickPickedCard(touch.position);
            }
        }
    }

    public void OnTouchEnd(LdTouch touch) {
        Vector2 normalisedMousePosition = LdMath2d.FromScreenToNormalisedScreenPoint(Screen.width, Screen.height, touch.position);

        if (!(normalisedMousePosition.y <= _handHitbox)) {
            if (GotDraggedCard()) {
                DropCard(iDraggedCard);
            }
        } else {
            if (!_hasJustDragged) {
                if (GotFocusedCard()) {
                    UnfocusCard(iFocusedCard, true);
                    _isAlreadyChosen = false;
                }
            } else {
                UnfocusCard(iFocusedCard, true);

            }
        }
        _focusTime = 0;
    }


    #endregion // Controls

    #region Utils


    private int FromPositionToCardIndex(float position) {
        int iCardIndex;
        int nCardCount = 3;
        position *= 1.40f; // Move selection to the left
        position = Mathf.Clamp01(position);
        iCardIndex = Mathf.FloorToInt(position * nCardCount);
        iCardIndex = Mathf.Clamp(iCardIndex, 0, nCardCount - 1);
        return iCardIndex;
    }

    #endregion


}