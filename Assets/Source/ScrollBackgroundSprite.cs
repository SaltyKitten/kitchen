﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBackgroundSprite : MonoBehaviour {
    private float size;
    private float originZ;
    // Use this for initialization
    void Start() {
        SpriteRenderer sp = this.GetComponent<SpriteRenderer>();
        size = sp.bounds.max.y - sp.bounds.min.y;

    }

    // Update is called once per frame
    void Update() {

        if (this.transform.position.y >= size)
            this.transform.position = new Vector3(0, -size+0.5f, 10);
        else
            this.transform.position = this.transform.position + new Vector3(0, 0.01f, 0); //TODO FIX


    }
}
