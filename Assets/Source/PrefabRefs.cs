﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabRefs : MonoBehaviour {

    public static PrefabRefs instance;

    public Sprite candyA;
    public Sprite candyB;
    public Sprite candyC;
    public Sprite fusion;
    public GameObject entityPrefab;
    public GameObject[] monsterPrefabs;
    public GameObject lifePrefab;

    public void Awake() {
        instance = this;
    }

    public GameObject GetMonsterPrefab() {
        return monsterPrefabs[ Random.Range(0, monsterPrefabs.Length)];

    }

}

