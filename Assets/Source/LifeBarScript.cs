﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBarScript : MonoBehaviour {
    private const float animDuration = 0.40f;
    public List<GameObject> lifeBar = new List<GameObject>();

    public void Init(int size) { 
        foreach(GameObject go in lifeBar) {
            Destroy(go);
        }
        lifeBar.Clear();

        for (int i = 0; i<size;i++) {
            GameObject livePoint = Instantiate(PrefabRefs.instance.lifePrefab, this.transform.position +  new Vector3( i * 0.5f, -1),Quaternion.identity);
            livePoint.transform.SetParent(this.transform);
            lifeBar.Add(livePoint);
        }

    }

    public void LooseLives() { 
        if (lifeBar.Count > 0) {
            GameObject token = lifeBar[lifeBar.Count - 1];
            lifeBar.RemoveAt(lifeBar.Count-1);
            token.transform.transform.DOShakePosition(animDuration, 0.25f, 100, 45, false, true);
            token.transform.transform.DOScale(0, animDuration).onComplete =delegate() {
                Destroy(token);
            };
        }
    }

}
