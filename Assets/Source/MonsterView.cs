﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterView : MonoBehaviour {

    private MonsterModel _model;

    public GameObject waiting;
    public GameObject angry;
    public GameObject happy;

    public SpriteRenderer wantedEntity;


    public void Init(MonsterModel model) {
        this._model = model;

        switch (model.expectedEntity) {
            case EntityModel.EntityType.A:
                wantedEntity.sprite = PrefabRefs.instance.candyA;
                break;
            case EntityModel.EntityType.B:
                wantedEntity.sprite = PrefabRefs.instance.candyB;
                break;
            case EntityModel.EntityType.C:
                wantedEntity.sprite = PrefabRefs.instance.candyC;
                break;
        }
    }


    public void SetHappy() {
        waiting.SetActive(false);
        happy.SetActive(true);
        angry.SetActive(false);
    }

    public void SetAngry() {
        waiting.SetActive(false);
        happy.SetActive(false);
        angry.SetActive(true);
    }

    public void SetWaiting() {
        waiting.SetActive(true);
        happy.SetActive(false);
        angry.SetActive(false);
    }

}
