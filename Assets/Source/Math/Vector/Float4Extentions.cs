﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LibelludDigital.Math
{
    public static class Float4Extentions
    {
        // Vector4

        public static Vector4 XYZW(this Vector4 v4)
        {
            return v4;
        }

        public static Vector4 XYWZ(this Vector4 v4)
        {
            return new Vector4(v4.x, v4.y, v4.w, v4.z);
        }


    }
}