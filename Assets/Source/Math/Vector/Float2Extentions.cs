﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LibelludDigital.Math
{
    public static class LdMath2d
    {
        /// <summary>
        /// Remap a vector so that [when input = min, output = 0} and [when input = max, output = 1].
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector2 InverseLerp(Vector2 min, Vector2 max, Vector2 v)
        {
            return new Vector2(
                LdMath.InverseLerp(min.x, max.x, v.x),
                LdMath.InverseLerp(min.y, max.y, v.y)
                );
        }

        public static Vector2 Project(Vector2 vector, Vector2 onNormal)
        {
            float normalLengthSquared = onNormal.magnitude;
            normalLengthSquared = normalLengthSquared * normalLengthSquared;
            return onNormal * (Vector2.Dot(onNormal, vector) / normalLengthSquared);
        }

        
        public static Vector2 FromScreenToAnchorPoint(RectTransform rectTransform, Vector2 cursorPositionScreen)
        {
            Canvas c = rectTransform.GetComponentInParent<Canvas>();
            RectTransform r = (RectTransform)c.transform;
            Vector2 canvasSize = r.rect.size;
            Vector2 realSize = c.pixelRect.size;
            Vector2 scaleRatio = new Vector2(realSize.x / canvasSize.x, realSize.y / canvasSize.y);
            Rect rect = rectTransform.rect;
            rect.position -= rect.position;
            rect.Set(rect.x * scaleRatio.x, rect.y * scaleRatio.y, rect.width * scaleRatio.x, rect.height * scaleRatio.y);
            return LdMath2d.InverseLerp(rect.min, rect.max, cursorPositionScreen);
        }

        public static Vector2 FromScreenToNormalisedScreenPoint(float screenWidth, float screenHeight, Vector2 screenPoint)
        {
            return new Vector2(
                screenPoint.x / screenWidth,
                screenPoint.y / screenHeight
            );
        }

        public static Vector2 FromScreenToNormalisedScreenPoint(Vector2 screnSize, Vector2 screenPoint)
        {
            return new Vector2(
                screenPoint.x / screnSize.x,
                screenPoint.y / screnSize.y
            );
        }
    }

    public static class Vector2Extentions
    {
        public static Vector2 XY(this Vector2 v2)
        {
            return v2;
        }

        public static Vector2 YX(this Vector2 v2)
        {
            return new Vector2(v2.y, v2.x);
        }

        public static Vector3 XY(this Vector3 v3)
        {
            return new Vector2(v3.x, v3.y);
        }

        public static Vector3 YX(this Vector3 v3)
        {
            return new Vector2(v3.y, v3.x);
        }

        public static Vector3 XY(this Vector4 v4)
        {
            return new Vector2(v4.x, v4.y);
        }

        public static Vector3 YX(this Vector4 v4)
        {
            return new Vector2(v4.y, v4.x);
        }
    }
}