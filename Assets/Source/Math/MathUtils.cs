﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LibelludDigital.Math
{
    public static class LdMath
    {
        public const float InchToCentimeters = 2.54f;
        public const float CentimetersToInch = 1f / 2.54f;
        public static float PixelsToCentimeters { get { return InchToCentimeters / Screen.dpi; } }

        public static float InverseLerp(float min, float max, float x)
        {
            return (x - min) / (max - min);
        }
    }
}