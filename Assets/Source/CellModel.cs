﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellModel {
    
}

public class EntityCellModel : CellModel
{
    public int iBottomCardSlotIndex = -1;
    public int iTopCardSlotIndex = -1;

    public EntityModel entity;
    
}

public class CardSlotModel : CellModel
{

    public CardModel card;

}