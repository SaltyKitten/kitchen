﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel {

    public const int CELL_COUNT_X = 5;
    public const int CELL_COUNT_Y = 3;
    public const int CELL_COUNT = CELL_COUNT_X * CELL_COUNT_Y;
    public const int CARD_SLOT_COUNT_X = 3;
    public const int CARD_SLOT_COUNT_Y = 2;
    public const int CARD_SLOT_COUNT = CARD_SLOT_COUNT_X * CARD_SLOT_COUNT_Y;
    private const int START_LIVES = 10;

    public int MIN_DELAY_RESPAWN = 5;
    public int MAX_DELAY_RESPAWN = 7;

    public int lives;
    public int goodPoints;

    public List<EntityModel> activeEnities = new List<EntityModel>();
    public List<CardModel> activeCards = new List<CardModel>();
    public EntityCellModel[] cells;
    public CardSlotModel[] cardSlots;
    public MonsterModel[] monsters;
    public MonsterModel[] monsterApproachings;
    public int[] monsterApproachingDelays;

    public CardModel[] hand;

    public delegate void OnCardDrawn(int iCard);
    public event OnCardDrawn eOnCardDrawn;

    public void Init() {
        // Init card slots
        cardSlots = new CardSlotModel[CARD_SLOT_COUNT];
        for (int iCardSlot = 0; iCardSlot < CARD_SLOT_COUNT; iCardSlot++) {
            cardSlots[iCardSlot] = new CardSlotModel();
        }

        // Init hand
        hand = new CardModel[3];

        // Init cells
        cells = new EntityCellModel[CELL_COUNT];
        int xCell, yCell;
        for (int iCell = 0; iCell < CELL_COUNT; iCell++) {
            cells[iCell] = new EntityCellModel();
            FromIndexToTwoDimentional(iCell, CELL_COUNT_X, out xCell, out yCell);
            if (xCell > 0 && xCell < CELL_COUNT_X - 1) {
                // If not to the extream left or the extream right, might got some adgacent card slots.
                if (yCell > 0) {
                    // If not bottom line, got adgacent bottom card slot.
                    cells[iCell].iBottomCardSlotIndex = ((iCell / CELL_COUNT_X) - 1) * CARD_SLOT_COUNT_X + (xCell - 1); // Math magic
                }
                if (yCell < CELL_COUNT_Y - 1) {
                    // If not top line, got adgacent top card slot.
                    cells[iCell].iTopCardSlotIndex = ((iCell / CELL_COUNT_X)) * CARD_SLOT_COUNT_X + (xCell - 1); // Math magic
                }
            }
        }

        monsters = new MonsterModel[3];
        monsterApproachings = new MonsterModel[3];
        monsterApproachingDelays = new int[3];

        for (int i = 0; i < 3; i++) {

            monsters[i] = new MonsterModel();

        }

        goodPoints = 0;
        lives = START_LIVES;
    }

    public void StartGame() {
        DrawMissingCards();
    }
    private void Respawn(int line) {
        monsterApproachings[line] = new MonsterModel();
        monsterApproachingDelays[line] = Random.Range(MIN_DELAY_RESPAWN, MAX_DELAY_RESPAWN + 1);

    }
    public void EndGame() {

    }

    public void TickMovementResolution() {
        // Each tick
        int nActiveEntities = activeEnities.Count;
        EntityModel entity;
        int iCell;
        CardModel[] adgacentCards;
        CardModel adgacentCard;
        bool useDefaultMovement;
        EntityCellModel cell;

        // Move each active entities
        for (int iActiveEntity = nActiveEntities - 1; iActiveEntity >= 0; --iActiveEntity) {

            // For each entities
            entity = activeEnities[iActiveEntity];

            // MOVEMENT
            // Use default movement by default, unless a card movement effect is applied.
            useDefaultMovement = true;
            iCell = FromTwoDimentionalToIndex(entity.positionX, entity.positionY, CELL_COUNT_X);
            try {
                cell = cells[iCell];
                if (!cardSlots[cell.iTopCardSlotIndex].card.cancelledBot) {
                    useDefaultMovement &= !TryApplyMovement(cardSlots[cell.iTopCardSlotIndex].card, entity);
                    useDefaultMovement = false;
                }
                cardSlots[cell.iTopCardSlotIndex].card.movementEffectWasApplied = true;
            } catch {

            }
            try {
                cell = cells[iCell];
                if (!cardSlots[cell.iBottomCardSlotIndex].card.cancelledTop) {
                    useDefaultMovement &= !TryApplyMovement(cardSlots[cell.iBottomCardSlotIndex].card, entity);
                    useDefaultMovement = false;
                }
                cardSlots[cell.iBottomCardSlotIndex].card.movementEffectWasApplied = true;
            } catch {

            }

            // Get adgacent cards
            // START

            // END
            //adgacentCards = GetAdgacentCards(iCell);
            //if (adgacentCards != null) {
            //    for (int iAdgacentCard = 0; iAdgacentCard < adgacentCards.Length; iAdgacentCard++) {
            //        // For each adgacent cards
            //        adgacentCard = adgacentCards[iAdgacentCard];
            //        // Try applying it's movement effect to the entity.
            //        // If a movement effect was succesfully applyed, do no use the default movement.
            //        useDefaultMovement &= !TryApplyMovement(adgacentCard, entity);
            //    }
            //}
            if (useDefaultMovement) {
                // Move entity to the right by default.
                entity.Move(1, 0, false);
            }
        }
    }

    public void TickCombinaisonResolution() {
        HandleFusion();
    }

    public bool TickStateResolution() {
        // Each tick
        int nActiveEntities = activeEnities.Count;
        EntityModel entity;

        // Move each active entities
        for (int iActiveEntity = nActiveEntities - 1; iActiveEntity >= 0; --iActiveEntity) {
            // For each entities
            entity = activeEnities[iActiveEntity];

            // CHANGE STATE
            if (entity.positionY < 0 || entity.positionY >= CELL_COUNT_Y) {
                // The entity was thrown away.
                entity.SetState(EntityModel.StateEnum.TRASHED);
            }
            else if (entity.positionX == 4 && entity.positionY >= 0 && entity.positionY < CELL_COUNT_Y) {
                MonsterModel model = monsters[entity.positionY];
                if (model == null)
                    entity.SetState(EntityModel.StateEnum.TRASHED);
                else {
                    if (model.expectedEntity == entity.type || entity.type == EntityModel.EntityType.FUSION) {
                        entity.SetState(EntityModel.StateEnum.EATEN_GOOD);
                        goodPoints++;
                    }
                    else {
                        entity.SetState(EntityModel.StateEnum.EATEN_BAD);
                        lives--;
                    }
                    monsters[entity.positionY].hasEaten = true;
                    Respawn(entity.positionY);
                }
            }

            // CLEANING
            // Clean non alive entities
            if (entity.state != EntityModel.StateEnum.ALIVE) {
                activeEnities.RemoveAt(iActiveEntity);
            }

            // MONSTERS INCOMMINGS TURN ACTIVE
        }
        for (int i = 0; i < 3; i++) {

            if (monsters[i] != null && monsters[i].hasEaten)
                monsters[i] = null;


            if (monsters[i] == null && monsterApproachingDelays[i] == 0) {
                monsters[i] = monsterApproachings[i];
                monsterApproachings[i] = null;
                monsterApproachingDelays[i] = -1;
            }
            else {
                monsterApproachingDelays[i]--;
            }
        }
        return lives > 0;
    }

    public void TickCardConsumption() {
        // REFRESH CARDS
        int nActiveCards = activeCards.Count;
        CardModel activeCard;
        for (int iActiveCard = nActiveCards - 1; iActiveCard >= 0; --iActiveCard) {
            // For each active cards
            activeCard = activeCards[iActiveCard];
            if (activeCard.TryConsume()) {
                activeCards.RemoveAt(iActiveCard);
            }
        }
        for (int iSlot = 0; iSlot < cardSlots.Length; iSlot++) {/// CardSlotModel slot in cardSlots) {
            CardSlotModel slot = cardSlots[iSlot];
            if (slot.card != null) {
                if (slot.card.TryConsume()) {

                    try {
                        // Check if under card
                        if (cardSlots[iSlot - CARD_SLOT_COUNT_X].card != null) {
                            cardSlots[iSlot - CARD_SLOT_COUNT_X].card.UnCancelTop();
                            //cardSlots[iSlot].card.UnCancelBot();
                        }
                    } catch {

                    }
                    try {
                        // Check if upper card
                        if (cardSlots[iSlot + CARD_SLOT_COUNT_X].card != null) {
                            //cardSlots[iSlot].card.UnCancelTop();
                            cardSlots[iSlot + CARD_SLOT_COUNT_X].card.UnCancelBot();
                        }
                    } catch {

                    }

                    activeCards.Remove(slot.card);
                    slot.card = null;
                }
            }
        }
    }

    public void HandleFusion() {
        EntityCellModel cell;
        EntityModel entity;
        List<EntityModel> toRemove = new List<EntityModel>();
        int nActiveEntities = activeEnities.Count;
        for (int iCell = 0; iCell < cells.Length; iCell++) {
            cell = cells[iCell];
            cell.entity = null; // Clean
            for (int iActiveEntity = 0; iActiveEntity < nActiveEntities; iActiveEntity++) {
                entity = activeEnities[iActiveEntity];
                if (iCell == FromTwoDimentionalToIndex(entity.positionX, entity.positionY, CELL_COUNT_X)) {
                    // The position of the entity correspond to the index of the cell. Therefore the entity is on the cell.
                    if (cell.entity != null) {
                        cell.entity.Fusion();
                        toRemove.Add(entity);
                        //cell.entity = entity; // debug : do naugh.
                    }
                    else {
                        // No other entity is on the cell.
                        cell.entity = entity;
                    }
                }
            }
        }
        foreach (EntityModel model in toRemove) {
            model.eErased();
            activeEnities.Remove(model);
        }
    }

    public static bool TryApplyMovement(CardModel fromCard, EntityModel toEntity) {
        if (fromCard.consumed) return false; // Don't reapply an already applied movement
        toEntity.Move(fromCard.moveX, fromCard.moveY, true);
        fromCard.movementEffectWasApplied = true;
        return true;
    }

    public EntityModel MakeEntity() {
        EntityModel entity = new EntityModel();
        return entity;
    }

    public void SpawnEntity(EntityModel entity, int spawnX, int spawnY) {
        entity.Spawn(spawnX, spawnY);
        activeEnities.Add(entity);
    }

    public EntityCellModel GetCell(int x, int y) {
        return cells[FromTwoDimentionalToIndex(x, y, CELL_COUNT_X)];
    }

    public void PlayCard(int iCard, int iSlot) {
        activeCards.Add(hand[iCard]);
        if(cardSlots[iSlot].card != null) {
            cardSlots[iSlot].card.Consume();
        }
        cardSlots[iSlot].card = hand[iCard];
        try {
            // Check if under card
            if (cardSlots[iSlot - CARD_SLOT_COUNT_X].card != null) {
                cardSlots[iSlot - CARD_SLOT_COUNT_X].card.CancelTop();
                cardSlots[iSlot].card.CancelBot();
            }
        } catch {

        }
        try {
            // Check if upper card
            if (cardSlots[iSlot + CARD_SLOT_COUNT_X].card != null) {
                cardSlots[iSlot].card.CancelTop();
                cardSlots[iSlot + CARD_SLOT_COUNT_X].card.CancelBot();
            }
        } catch {

        }
        hand[iCard] = null;
        DrawMissingCards();
    }

    public CardModel MakeCard() {
        CardModel card = new CardModel();
        // Set random card
        card.SetMoveType((CardModel.MoveType)Random.Range(0, System.Enum.GetNames(typeof(CardModel.MoveType)).Length));
        return card;
    }

    public void DrawMissingCards() {
        for (int iCard = 0; iCard < hand.Length; ++iCard) {
            if (hand[iCard] == null) {
                hand[iCard] = MakeCard();

                if (eOnCardDrawn != null)
                    eOnCardDrawn(iCard);
            }
        }
    }

    public static int FromTwoDimentionalToIndex(int x, int y, int width) {
        return y * width + x;
    }

    public static void FromIndexToTwoDimentional(int iIndex, int width, out int x, out int y) {
        x = iIndex % width;
        y = iIndex / width;
    }

    public static bool IsIndexInsideArray(int index, int width, int heigth) {
        return index % width < 0 || index % width >= width || index / heigth < 0 || index / heigth >= heigth;
    }

    public static bool IsTwoDimentionalInsideArray(int x, int y, int width, int heigth) {
        return x < 0 || x >= width || y < 0 || y >= heigth;
    }

    public CardModel[] GetAdgacentCards(EntityCellModel cell) {

        if (cell.iBottomCardSlotIndex >= 0 && cardSlots[cell.iBottomCardSlotIndex].card != null) {
            if (cell.iTopCardSlotIndex >= 0 && cardSlots[cell.iTopCardSlotIndex].card != null) {
                CardModel[] adgacentCards = new CardModel[2];
                adgacentCards[0] = cardSlots[cell.iBottomCardSlotIndex].card;
                adgacentCards[1] = cardSlots[cell.iTopCardSlotIndex].card;
                return adgacentCards;
            }
            else {
                CardModel[] adgacentCards = new CardModel[1];
                adgacentCards[0] = cardSlots[cell.iBottomCardSlotIndex].card;
                return adgacentCards;
            }
        }
        else {
            if (cell.iTopCardSlotIndex >= 0 && cardSlots[cell.iTopCardSlotIndex].card != null) {
                CardModel[] adgacentCards = new CardModel[1];
                adgacentCards[0] = cardSlots[cell.iTopCardSlotIndex].card;
                return adgacentCards;
            }
            else {
                return null;
            }
        }

    }

    public CardModel[] GetAdgacentCards(int iCellIndex) {
        return GetAdgacentCards(cells[iCellIndex]);
    }

}
