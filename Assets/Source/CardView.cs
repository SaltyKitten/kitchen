﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class CardView : MonoBehaviour {

    public CardModel model;

    public SpriteRenderer token;
    public SpriteRenderer top;
    public SpriteRenderer bottom;

    [Header("Slot")]
    public Sprite freezeSprite;
    public Sprite topLeftSprite;
    public Sprite topSprite;
    public Sprite topRightSprite;
    public Sprite bottomLeftSprite;
    public Sprite bottomSprite;
    public Sprite bottomRightSprite;
    
    [Header("Mark Top")]
    public Sprite topMarkfreezeSprite;
    public Sprite topMarktopLeftSprite;
    public Sprite topMarktopSprite;
    public Sprite topMarktopRightSprite;
    public Sprite topMarkbottomLeftSprite;
    public Sprite topMarkbottomSprite;
    public Sprite topMarkbottomRightSprite;
    [Header("Mark BOTTOM")]
    public Sprite botMarkfreezeSprite;
    public Sprite botMarktopLeftSprite;
    public Sprite botMarktopSprite;
    public Sprite botMarktopRightSprite;
    public Sprite botMarkbottomLeftSprite;
    public Sprite botMarkbottomSprite;
    public Sprite botMarkbottomRightSprite;
    [Header("Marlopez Cancel")]
    public Sprite cancelMarkSprite;

    public void Init(CardModel model) {
        this.model = model;
        

        model.eOnConsomption = OnConsumed;
        model.eOnCancelBot = CancelBot;
        model.eOnUnCancelBot = UnClancelBot;
        model.eOnCancelTop = CancelTop;
        model.eOnUnCancelTop = UnClancelTop;
        ApplySprite();
        top.DOFade(0f, 0f);
        bottom.DOFade(0f, 0f);
    }

    public void ApplySprite() {

        switch (model.moveType) {
            case CardModel.MoveType.FREEZE:
                token.sprite = freezeSprite;
                top.sprite = topMarkfreezeSprite;
                bottom.sprite = botMarkfreezeSprite;
                break;
            case CardModel.MoveType.TOP_LEFT:
                token.sprite = topLeftSprite;
                top.sprite = topMarktopLeftSprite;
                bottom.sprite = botMarktopLeftSprite;
                break;
            case CardModel.MoveType.TOP:
                token.sprite = topSprite;
                top.sprite = topMarktopSprite;
                bottom.sprite = botMarktopSprite;
                break;
            case CardModel.MoveType.TOP_RIGHT:
                token.sprite = topRightSprite;
                top.sprite = topMarktopRightSprite;
                bottom.sprite = botMarktopRightSprite;
                break;
            case CardModel.MoveType.BOTTOM_LEFT:
                token.sprite = bottomLeftSprite;
                top.sprite = topMarkbottomLeftSprite;
                bottom.sprite = botMarkbottomLeftSprite;
                break;
            case CardModel.MoveType.BOTTOM:
                token.sprite = bottomSprite;
                top.sprite = topMarkbottomSprite;
                bottom.sprite = botMarkbottomSprite;
                break;
            case CardModel.MoveType.BOTTOM_RIGHT:
                token.sprite = bottomRightSprite;
                top.sprite = topMarkbottomRightSprite;
                bottom.sprite = botMarkbottomRightSprite;
                break;
        }
    }

    public void OnPlay() {
        Color color = Color.white;
        color.a = 0;
        bottom.color = color;
        top.DOFade(0.5f, 0.25f);
        bottom.DOFade(0.5f, 0.25f);
    }

    public void CancelBot() {
        bottom.sprite = cancelMarkSprite;
        //Color red = Color.red;
        DOTween.Kill(bottom);
        Color color = Color.red;
        color.a = 0;
        bottom.color = color;
        bottom.DOFade(0.75f, 0.25f);
    }

    public void UnClancelBot() {
        if (model.consumed) return;
        Color color = Color.white;
        color.a = 0;
        bottom.color = color;
        bottom.DOFade(0.5f, 0.25f);
        ApplySprite();
    }

    public void CancelTop() {
        top.sprite = null;
    }
    public void UnClancelTop() {
        if (model.consumed) return;
        Color color = Color.white;
        color.a = 0;
        top.color = color;
        top.DOFade(0.5f, 0.25f);
        ApplySprite();
    }

    public void OnConsumed() {
        transform.DOKill();
        token.transform.parent.DOScale(0f, 0.5f);
        token.transform.parent.DORotate(Vector3.forward * 180 * (Random.value * 2 - 1), 0.5f);
        top.DOFade(0f, 0.125f);
        bottom.DOFade(0f, 0.125f);
        Destroy(this.gameObject, 1f);
    }
}
